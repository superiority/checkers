/**
 * Created by Feodor on 28.11.2015.
 */

var currentChecker = null;
var i = null;
var j = null;
var player = null;

function editAjax(nameId,passId,target) {

    var xhr = new XMLHttpRequest();
    var name = document.getElementById(nameId);
    var pass = document.getElementById(passId);
    if (name.value.length < 1 || name.value.length > 12)
    {
        alert("Длина имени должна быть от 1 до 12 символов");
        return;
    }
    if (pass.value.length < 2 || pass.value.length > 8 )
    {
        alert("Длина пароля должна быть от 3 до 8 символов");
        return;
    }
    var params = 'name=' + name.value + '&pass=' + pass.value + '&target=' + target;

    xhr.timeout = 5000;
    xhr.ontimeout = function() {
        alert( 'Извините, запрос превысил максимальное время' );
    };

    xhr.open('POST', 'pages/ajax.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");

    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;

        if (xhr.status != 200) {
            alert(xhr.status + ': ' + xhr.statusText);
        } else {
            if (target = 'reg') {
                if (xhr.responseText == '<div class="alert alert-success" role="alert">Регистрация успешно завершена!</div>') {
                    regBody.innerHTML = xhr.responseText;
                    reg.style.height = "200pt";
                    jsSubmitReg.style.display = 'none';
                    document.cookie = "username=" + name.value;
                    location.reload();
                }
                else
                    jsAjaxReg.innerHTML = xhr.responseText;
            }

            if (target = 'log') {
                if (xhr.responseText == '<div class="alert alert-success" role="alert">Вход успешно выполнен!</div>') {
                    logBody.innerHTML = xhr.responseText;
                    log.style.height = "200pt";
                    jsSubmitLog.style.display = 'none';
                    document.cookie = "username=" + name.value;
                    location.reload();
                }
                else
                    jsAjaxLog.innerHTML = xhr.responseText;
            }
        }
    };

    xhr.send(params);
}

function exit()
{
    var cookie_date = new Date ( );  // Текущая дата и время
    cookie_date.setTime ( cookie_date.getTime() - 1 );
    document.cookie = "username" + "=; expires=" + cookie_date.toGMTString();
    location.reload();
}

function clickAlly(className,number,row,collumn)
{
    checks = document.getElementsByClassName(className);
    for (var k = 0; k < checks.length; k++)
    {
        checks[k].parentNode.style.border = '2pt solid rgba(0,0,0,0)';
        if (k == number) {
            checks[k].parentNode.style.border = '2pt solid yellow';
            window.currentChecker =  checks[k];
        }
    }
    window.i = row;
    window.j = collumn;

    if((className == 'jswhiteChecker' || className == 'jswhiteQueenChecker'))
        window.player = 1;
    else
        window.player = -1;
}

function update()
{
    var timerId = setTimeout(checkStatus, 2000);
}

function checkStatus()
{
    getNewInfo();

    var username = 'Guest';
    if (getCookie('username') != undefined)
        username = getCookie('username');
    if (getCookie('gameId') == undefined)
        return;

    if (window.player == null)
    {
        var checker = document.getElementsByClassName('jswhiteChecker')[0];
        if (!checker)
            checker = document.getElementsByClassName('jswhiteQueenChecker')[0];
        if (!checker)
            return;

        if (checker.classList.contains('jsAlly'))
            window.player = 1;
        else
            window.player = -1;
    }

    $.post(
        "../pages/checkersAjax.php",
        {
            state: "state",
            player: window.player,
            username: encodeURIComponent(username),
            gameId: encodeURIComponent(getCookie('gameId'))
        },
        function(data) {
            //alert(data);
            if (data == 'ERROR' || data == '')
                return;
            if (data == 'end')
            {
                document.location.href = "../index.php";
                return;
            }
            tableBody.innerHTML = data;
            if (window.currentChecker)
                window.currentChecker.click();
        }
    );

    setTimeout(checkStatus, 2000);
}

function clickEmpty(row,column)
{
    if (window.currentChecker === null)
        return;

    //Проверка на корректность хода.
    if (window.i - row == 1 * window.player && Math.abs(window.j - column) == 1)
    {
        var username = 'Guest';
        if (getCookie('username') != undefined)
            username = getCookie('username');
        if (getCookie('gameId') == undefined)
            return;

        $.post(
            "../pages/checkersAjax.php",
            {
                step: "step",
                checkerI: window.i,
                checkerJ: window.j,
                targetI: row,
                targetJ: column,
                player: window.player,
                username: encodeURIComponent(username),
                gameId: encodeURIComponent(getCookie('gameId'))
            },
            function(data) {
                if (data == 'ERROR')
                    return
                tableBody.innerHTML = data;
                //alert(data);
            }
        );
    }
}

function clickEnemy(className,number,row,column)
{
    if (window.currentChecker === null)
        return;

    var username = 'Guest';
    if (getCookie('username') != undefined)
        username = getCookie('username');
    if (getCookie('gameId') == undefined)
        return;

    $.post(
        "../pages/checkersAjax.php",
        {
            attack: "attack",
            checkerI: window.i,
            checkerJ: window.j,
            targetI: row,
            targetJ: column,
            player: window.player,
            username: encodeURIComponent(username),
            gameId: encodeURIComponent(getCookie('gameId'))
        },
        function(data) {
            if (data == 'ERROR' || data == '')
                return;
            tableBody.innerHTML = data;
            //alert(data);
        }
    );

}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function getNewInfo()
{
    if (getCookie('gameId') == undefined)
        return;

    $.post(
        "../pages/checkersAjax.php",
        {
            info: "info",
            gameId: encodeURIComponent(getCookie('gameId'))
        },
        function(data) {
            if (data == 'ERROR' || data == '')
                return;
            else
            {
                info.innerHTML = data;
            }
        }
    );
}