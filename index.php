<!DOCTYPE html>
<html lang="ru">
<head>
    <link href="css/styles.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="css/styles.css">
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <script src="scripts/scripts.js"></script>
    <meta charset="UTF-8">
    <title>Checkers</title>
</head>
<body>
<div class="container general">
    <div class="header clearfix">
        <nav>
            <ul class="nav nav-pills pull-right">
                <?php
                if (isset($_COOKIE['username'])) {
                    $username = $_COOKIE['username'];
                    echo "<li role='presentation'><h4><span class='label label-default' id='userName'>$username</span></h4></li>";
                    echo "<li role='presentation'><a class='btn' onclick='exit()'>Выйти</a></li>";
                } else
                    echo <<<_END
                    <li role="presentation"><a href="#reg" role="button" class="btn" data-toggle="modal">Регистрация</a></li>
                    <li role="presentation"><a href="#log" role="button" class="btn" data-toggle="modal">Войти</a></li>
_END;
                ?>
            </ul>
        </nav>
        <h3 class="text-muted">Checkers online</h3>
    </div>

    <div class="modal fade modal-window" id="reg" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Регистрация</h3>
        </div>
        <div class="modal-body" id="regBody">
            <div class="form-group">
                <label for="nameInput">Ваше имя</label>
                <input type="text" class="form-control" id="nameInput" placeholder="Имя" name="name">
            </div>
            <div class="form-group">
                <label for="passwordInput">Пароль</label>
                <input type="password" class="form-control" id="passwordInput" placeholder="Пароль" name="pass">
            </div>
            <div id="jsAjaxReg"></div>
        </div>

        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>
            <button class="btn btn-primary" id="jsSubmitReg" onclick="editAjax('nameInput','passwordInput','reg')">Сохранить</button>
        </div>
    </div>

    <div class="modal fade modal-window" id="log" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3>Войти</h3>
        </div>
        <div class="modal-body" id="logBody">
            <div class="form-group">
                <label for="nameInput2">Ваше имя</label>
                <input type="text" class="form-control" id="nameInput2" placeholder="Имя" name="name">
            </div>
            <div class="form-group">
                <label for="passwordInput2">Пароль</label>
                <input type="password" class="form-control" id="passwordInput2" name="pass" placeholder="Пароль">
            </div>
            <div id="jsAjaxLog"></div>
        </div>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Закрыть</button>
            <button class="btn btn-primary" id="jsSubmitLog" onclick="editAjax('nameInput2','passwordInput2','log')">Войти</button>
        </div>
    </div>

    <div class="jumbotron picture">
        <p><a class="btn btn-lg btn-default pull-right" href="pages/" role="button">Играть</a></p>
    </div>
    <section class="statistic panel panel-default">
        <?php
        if (isset($_COOKIE['username'])) {

            echo "<div class='panel-heading'>История игр</div> <div class='panel-body'>";

            require_once "pages/login.php";

            $username = $_COOKIE['username'];
            $wonCount = 0;
            $count = 0;
            global $dbHostname, $dbDatabase, $dbUsername, $dbPassword;
            $dbServer = mysqli_connect($dbHostname, $dbUsername, $dbPassword);
            if (!$dbServer) die(mysqli_error($dbServer));
            mysqli_select_db($dbServer, $dbDatabase);
            mysqli_set_charset($dbServer, 'utf8');

            $sql = "SELECT
                        `gameId`,
                        `Player1`,
                        `Player2`,
                        `WINNER`
                    FROM
                        `game`
                    WHERE
                        (`PLAYER1` = '$username'
                        OR `Player2` = '$username' )
                        AND `WINNER` is NOT NULL
                    ORDER BY
                        `gameId` DESC";
            $result = mysqli_query($dbServer,$sql) or die(mysqli_error($dbServer));
            mysqli_close($dbServer);

            $count = mysqli_num_rows($result);
            if ($count < 1)
                echo "<div class='gameResult alert alert-info'>Вы еще не сыграли ниодной игры.</div>";


            $row = mysqli_fetch_row($result);
            while($row)
            {
                if ($row[3] == $username) {
                    echo "<div class='gameResult alert alert-success'><span class='pull-left'>$row[1]</span><span class='pull-right'>$row[2]</span></div>";
                    $wonCount++;
                }
                else
                    echo "<div class='gameResult alert alert-danger'><span class='pull-left'>$row[1]</span><span class='pull-right'>$row[2]</span></div>";
                $row = mysqli_fetch_row($result);
            }
            $temp = $count -$wonCount;
            echo "</div>";
            echo "<div class='gameResult alert alert-info' style='padding-bottom: 10pt'>Всего игр/Побед/Поражений | $count/$wonCount/$temp</div>";
        }
        ?>
    </section>
    <section>
    </section>
    <footer class="footer">
        <p>2015 Checkers online</p>
    </footer>
</div>
</body>
</html>