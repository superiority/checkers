<?php
/**
 * Created by PhpStorm.
 * User: Feodor
 * Date: 28.11.2015
 * Time: 17:26
 */

require_once 'login.php';
require_once 'functions.php';

if (isset($_POST['target']))
    if ($_POST['target'] == 'reg')
        registration();
    elseif($_POST['target'] == 'log')
        login();

function registration()
{

    if (!isset($_POST['name']) || !isset($_POST['pass']))
        return;

    $name = mysql_entities_fix_string($_POST['name']);
    $pass = mysql_entities_fix_string($_POST['pass']);

    if (strlen($name) < 1 || strlen($pass) < 2 || strlen($pass) > 8 || strlen($name) > 12) {
        echo '<div class="alert alert-danger" role="alert">Не корректные данные.</div>';
        return;
    }

    global $dbHostname, $dbDatabase, $dbUsername, $dbPassword;

    $dbServer = mysqli_connect($dbHostname, $dbUsername, $dbPassword);
    if (!$dbServer) die(mysqli_error($dbServer));
    mysqli_select_db($dbServer, $dbDatabase);
    mysqli_set_charset($dbServer, 'utf8');

    $result = mysqli_query($dbServer,"SELECT * FROM `player` WHERE `name`=\"$name\"");
    if ($result) {
        if (mysqli_num_rows($result) > 0)
            echo '<div class="alert alert-warning" role="alert">Такое имя уже занято!</div>';
        else {
            if (mysqli_query($dbServer, "INSERT INTO `player`(`Name`, `Pass`) VALUES (\"$name\",\"$pass\")"))
                echo '<div class="alert alert-success" role="alert">Регистрация успешно завершена!</div>';
            else
                echo '<div class="alert alert-danger" role="alert">Неудалось завершить регистрацию!</div>';
        }
    }
    else
        echo '<div class="alert alert-danger" role="alert">Не удалось проверить уникальность именни.</div>';

    mysqli_close($dbServer);
}

function login()
{
    if (!isset($_POST['name']) || !isset($_POST['pass']))
        return;

    $name = mysql_entities_fix_string($_POST['name']);
    $pass = mysql_entities_fix_string($_POST['pass']);

    if (strlen($name) < 1 || strlen($pass) < 2 || strlen($pass) > 8 || strlen($name) > 12) {
        echo '<div class="alert alert-danger" role="alert">Не корректные данные.</div>';
        return;
    }

    global $dbHostname, $dbDatabase, $dbUsername, $dbPassword;

    $dbServer = mysqli_connect($dbHostname, $dbUsername, $dbPassword);
    if (!$dbServer) die(mysqli_error($dbServer));
    mysqli_select_db($dbServer, $dbDatabase);
    mysqli_set_charset($dbServer, 'utf8');

    $result = mysqli_query($dbServer,"SELECT * FROM `player` WHERE `name`=\"$name\" AND `pass`=\"$pass\"");
    if ($result) {
        if (mysqli_num_rows($result) > 0)
        {
            echo '<div class="alert alert-success" role="alert">Вход успешно выполнен!</div>';
        }
        else {
            echo '<div class="alert alert-warning" role="alert">Имя или пароль не верны!</div>';
        }
    }
    else
        echo '<div class="alert alert-danger" role="alert">Не удалось проверить существование такого игрока.</div>';

    mysqli_close($dbServer);
}

