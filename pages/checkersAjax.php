<?php
/**
 * Created by PhpStorm.
 * User: Feodor
 * Date: 29.11.2015
 * Time: 16:23
 */

require_once 'login.php';
require_once 'functions.php';

if (isset($_POST['state']))
    checkState();
elseif (isset($_POST['step']))
    makeStep();
elseif (isset($_POST['info']))
    getNewInfo();
//elseif(isset($_POST['attack']))
    //attack();
/**
 * Проверка состояния поля. Если твой ход, ищет кого можно убить.
 */
function checkState()
{
    if (!isset($_POST['gameId']) || !isset($_POST['username']) || !isset($_POST['player'])) {
        echo 'ERROR';
        return;
    }

    global $dbHostname, $dbDatabase, $dbUsername, $dbPassword;
    $dbServer = mysqli_connect($dbHostname, $dbUsername, $dbPassword);
    if (!$dbServer) die(mysqli_error($dbServer));
    mysqli_select_db($dbServer, $dbDatabase);
    mysqli_set_charset($dbServer, 'utf8');

    $username = mysql_entities_fix_string($_POST['username']);
    $gameId = mysql_entities_fix_string($_POST['gameId']);

    //Проверка на законченость игры.
    $query = mysqli_query($dbServer, "SELECT `WINNER` FROM `GAME` WHERE `gameId`=$gameId") or die(mysqli_error($dbServer) . 'Попытка загрузки игры.');
    if (mysqli_fetch_row($query)[0] !== null)
    {
        setcookie ("gameId", "", time() - 3600);
        mysqli_close($dbServer);
        echo 'end';
        return;
    }

    $player = 'Player1';
    if ($_POST['player'] == -1) {
        $player = 'Player2';
    }

    $query = mysqli_query($dbServer, "SELECT * FROM `GAME` WHERE `GAMEID`=$gameId AND `$player`='$username'") or die(mysqli_error($dbServer));
    if (mysqli_num_rows($query) < 1) {
        echo "ERROR";
        mysqli_close($dbServer);
        return;
    }

    $game = mysqli_fetch_row($query);
    $state = json_decode($game[3], true);

    if (checkEndOfGame($state))
    {
        $winner = $game[2];
        if ($game[4] == $game[1])
            $winner = $game[1];

        $sql = "UPDATE
                    `game`
                SET
                    `turn` = 0,
                    `WINNER` = '$winner'
                WHERE `gameId` = $gameId";
        mysqli_query($dbServer,$sql);
        setcookie ("gameId", "", time() - 3600);
        mysqli_close($dbServer);
        //header("Location: ../index.php");
        echo 'end';
        return;
    }

    $color = 'white';
    if ($_POST['player'] == -1)
        $color = 'black';

    if (!isset($game[4]))
        return;
    if ($game[4] != $_POST['player'])
        return;

    $enemyNumbers = [3,4];
    $nextTurn = -1;
    if ($_POST['player'] == -1) {
        $nextTurn = 1;
        $enemyNumbers = [1,2];
    }

    if (checkForAttack($state,$enemyNumbers))
    {
        //если кого-то принудительно убили
        newStateEcho($state,$nextTurn,$color);
        $json = json_encode($state);
        mysqli_query($dbServer,"UPDATE `game` SET `state`='$json',`turn`=$nextTurn WHERE `gameId`=$gameId") or die(mysqli_error($dbServer));
        mysqli_close($dbServer);
        return;
    }

    mysqli_close($dbServer);
    newStateEcho($state,$game[4], $color);
}

/**
 * Если можно убить - бьет, иначе ходит.
 */
function makeStep()
{
    if (!isset($_POST['checkerI']) || !isset($_POST['checkerJ']) || !isset($_POST['targetI']) || !isset($_POST['targetJ']) || !isset($_POST['player'])) {
        echo "ERROR";
        return;
    }

    $enemyNumbers = [3,4];
    $player = 'Player1';
    $color = 'white';
    $nextTurn = -1;

    if ($_POST['player'] == -1) {
        $player = 'Player2';
        $color = 'black';
        $nextTurn = 1;
        $enemyNumbers = [1,2];
    }

    global $dbHostname, $dbDatabase, $dbUsername, $dbPassword;
    $dbServer = mysqli_connect($dbHostname, $dbUsername, $dbPassword);
    if (!$dbServer) die(mysqli_error($dbServer));
    mysqli_select_db($dbServer, $dbDatabase);
    mysqli_set_charset($dbServer, 'utf8');

    $username = mysql_entities_fix_string($_POST['username']);
    $gameId = mysql_entities_fix_string($_POST['gameId']);

    $query = mysqli_query($dbServer,"SELECT * FROM `GAME` WHERE `GAMEID`=$gameId AND `$player`='$username'") or die(mysqli_error($dbServer));
    if (mysqli_num_rows($query) < 1) {
        echo 'ERROR';
        mysqli_close($dbServer);
        return;
    }

    $game = mysqli_fetch_row($query);
    if ($game[4] != $_POST['player']) {
        echo 'ERROR';
        mysqli_close($dbServer);
        return;
    }

    $state = json_decode($game[3],true);

    if (checkForAttack($state,$enemyNumbers))
    {
        //если кого-то принудительно убили
        newStateEcho($state,$nextTurn,$color);
        $json = json_encode($state);
        mysqli_query($dbServer,"UPDATE `game` SET `state`='$json',`turn`=$nextTurn WHERE `gameId`=$gameId") or die(mysqli_error($dbServer));
    }
    elseif ($_POST['checkerI'] - $_POST['targetI'] == 1 * $_POST['player'] && abs($_POST['checkerJ'] - $_POST['targetJ']) == 1)
    {
        $checker = $state[$_POST['checkerI']][$_POST['checkerJ']];
        unset($state[$_POST['checkerI']][$_POST['checkerJ']]);

        if ($checker == 1 && $_POST['targetI'] == 0)
            $checker++;
        if ($checker == 3 && $_POST['targetI'] == 7)
            $checker++;

        $state[$_POST['targetI']][$_POST['targetJ']] = $checker;

        newStateEcho($state,$nextTurn,$color);
        $json = json_encode($state);
        mysqli_query($dbServer,"UPDATE `game` SET `state`='$json',`turn`=$nextTurn WHERE `gameId`=$gameId") or die(mysqli_error($dbServer));
    }
    mysqli_close($dbServer);
}

/**
 * Проверяет возможность бить вражеские шашки, если можно - бьет.
 * @param $state array Шашки на поле.
 * @param $enemyNumbers array Цифры шашек противника.
 * @return bool True если кого-то побили.
 */
function checkForAttack(&$state,$enemyNumbers)
{
    //print_r($enemyNumbers);
    for ($i = 0; $i < 8; $i++)
        for ($j = 0; $j < 8; $j++)
            if (isset($state[$i][$j]))
                if (array_search($state[$i][$j],$enemyNumbers) === false)
                    if (killNear($state, $enemyNumbers, $i, $j))
                        return true;
    return false;
}

/**
 * Проверяет возможность побить вражеские шашки для кокретной шашки, если можно - бьет.
 * @param $state array Шашки на поле.
 * @param $enemyNumbers array Цифры шашек противника.
 * @param $i int Ряд шашки для которой проверяем.
 * @param $j int Колонка шашки для которой проверяем.
 * @return bool True если кого-то побили.
 */
function killNear(&$state, $enemyNumbers, $i, $j)
{
    $killedSomeone = false;
    $ableToAttack = getAbleToKillNear($state,$enemyNumbers,$i,$j);

    if (count($ableToAttack) > 0)
    {
        $newPosition = kill($state, $i, $j,$ableToAttack[0][0],$ableToAttack[0][1]);
        $killedSomeone = true;
        killNear($state,$enemyNumbers,$newPosition[0],$newPosition[1]);
    }

    return $killedSomeone;
}

/**
 * Проверяет возможность побить вражеские шашки для кокретной шашки.
 * @param $state array Шашки на поле.
 * @param $enemyNumbers array Цифры шашек противника.
 * @param $chRow int Ряд шашки для которой проверяем.
 * @param $chCol int Колонка шашки для которой проверяем.
 * @return array Кординаты шашек которые можно побить.
 */
function getAbleToKillNear($state,$enemyNumbers,$chRow,$chCol)
{
    $ableToAttack = array();

    if (checkAbleToAttack($state,$enemyNumbers,$chRow,$chCol,1,1))
        $ableToAttack[] = [$chRow + 1,$chCol + 1];
    if (checkAbleToAttack($state,$enemyNumbers,$chRow,$chCol,1,-1))
        $ableToAttack[] = [$chRow + 1,$chCol - 1];
    if (checkAbleToAttack($state,$enemyNumbers,$chRow,$chCol,-1,1))
        $ableToAttack[] = [$chRow - 1,$chCol + 1];
    if (checkAbleToAttack($state,$enemyNumbers,$chRow,$chCol,-1,-1))
        $ableToAttack[] = [$chRow - 1,$chCol - 1];

    return $ableToAttack;
}

/**
 * Проверяет возможность побить вражескую шашку для кокретной шашки.
 * @param $state array Шашки на поле.
 * @param $enemyNumbers array Цифры шашек противника.
 * @param $chRow int Ряд нашей шашки для которой проверяем.
 * @param $chCol int Колонка нашей шашки для которой проверяем.
 * @param $modRow int Ряд шашки противника для которой проверяем.
 * @param $modCol int Колонка шашки противника для которой проверяем.
 * @return bool True если можно побить.
 */
function checkAbleToAttack($state,$enemyNumbers,$chRow,$chCol,$modRow,$modCol)
{
    $targetRow = $chRow + $modRow;
    $targetCol = $chCol + $modCol;
    if (isset($state[$targetRow][$targetCol]))
    {
        if ($state[$targetRow][$targetCol] == $enemyNumbers[0] || $state[$targetRow][$targetCol] == $enemyNumbers[1])
        {
            $targetRow +=$modRow;
            $targetCol +=$modCol;
            if (!isset($state[$targetRow][$targetCol]) && $targetRow < 8 && $targetCol < 8 && $targetRow >=0 && $targetCol>=0)
            {
                return true;
            }
        }
    }
    return false;
}

/**
 * Бьет шашку.
 * @param $state array Шашки на поле.
 * @param $i int Ряд нашей шашки.
 * @param $j int Колонка нашей шашки.
 * @param $targetI int Ряд шашки противника.
 * @param $targetJ int Колонка шашки противника.
 * @return array Новая позиция нашей шашки.
 */
function kill(&$state,$i,$j,$targetI,$targetJ)
{
    $newRow = abs($i - $targetI * 2);
    $newCol = abs($j - $targetJ * 2);

    $checker = $state[$i][$j];
    unset($state[$i][$j]);
    unset($state[$targetI][$targetJ]);

    if($checker == 1 && $newRow == 0)
        $checker++;
    if($checker == 3 && $newRow == 7)
        $checker++;

    $state[$newRow][$newCol] = $checker;

    return [$newRow,$newCol];
}

/*
function attack()
{
    if (!isset($_POST['checkerI']) || !isset($_POST['checkerJ']) || !isset($_POST['targetI']) || !isset($_POST['targetJ'])) {
        echo "ERROR";
        return;
    }

    if (abs($_POST['checkerJ'] - $_POST['targetJ']) == 1) {

        global $dbHostname, $dbDatabase, $dbUsername, $dbPassword;
        $dbServer = mysqli_connect($dbHostname, $dbUsername, $dbPassword);
        if (!$dbServer) die(mysqli_error($dbServer));
        mysqli_select_db($dbServer, $dbDatabase);
        mysqli_set_charset($dbServer, 'utf8');

        $username = mysql_entities_fix_string($_POST['username']);
        $gameId = mysql_entities_fix_string($_POST['gameId']);
        $player = mysql_entities_fix_string($_POST['player']);

        if ($_POST['player'] == 1)
            $player = 'Player1';
        elseif ($_POST['player'] == -1)
            $player = 'Player2';

        $query = mysqli_query($dbServer, "SELECT * FROM `GAME` WHERE `GAMEID`=$gameId AND `$player`='$username'") or die(mysqli_error($dbServer));
        if (mysqli_num_rows($query) < 1) {
            echo 'ERROR';
            mysqli_close($dbServer);
            return;
        }

        $game = mysqli_fetch_row($query);
        if ($game[4] != $_POST['player']) {
            echo 'ERROR';
            mysqli_close($dbServer);
            return;
        }

        $state = json_decode($game[3], true);

        $newRow = abs($_POST['checkerI'] - $_POST['targetI'] * 2);
        $newCol = abs($_POST['checkerJ'] - $_POST['targetJ'] * 2);

        if (isset($state[$newRow][$newCol]) || $newRow > 7 || $newCol > 7)
        {
            echo "ERROR";
            mysqli_close($dbServer);
            return;
        }

        $checker = $state[$_POST['checkerI']][$_POST['checkerJ']];
        unset($state[$_POST['checkerI']][$_POST['checkerJ']]);
        unset($state[$_POST['targetI']][$_POST['targetJ']]);

        if($checker == 1 && $newRow == 0)
            $checker++;
        if($checker == 3 && $newRow == 7)
            $checker++;

        $state[$newRow][$newCol] = $checker;

        $enemyNumbers = [1,2];
        if ($_POST['player'] == 1)
            $enemyNumbers = [3,4];

        //scanNear($state,$enemyNumbers,$newRow,$newCol);
        //counterStrike($state,$enemyNumbers,$newRow,$newCol);

        $color = 'white';
        $nextTurn = -1;
        if ($_POST['player'] == -1) {
            $color = 'black';
            $nextTurn = 1;
        }
        newStateEcho($state,$_POST['player'], $color);
        $json = json_encode($state);
        mysqli_query($dbServer,"UPDATE `game` SET `state`='$json',`turn`=$nextTurn WHERE `gameId`=$gameId") or die(mysqli_error($dbServer));
        mysqli_close($dbServer);
    }
}
*/

/**
 * Отображает поле.
 * @param $position array Шашки на поле.
 * @param $turn int Чей текущий ход (-1,1);
 * @param $color string Наш цвет.
 */
function newStateEcho($position,$turn, $color)
{
    $blackCount = $whiteCount = $blackQueenCount = $whiteQueenCount = 0;
    $youClasses = array($color . "Checker", $color .'Queen');

    for ($i = 0; $i < 8; $i++) {
        $temp = $i % 2;
        for ($j = 0; $j < 8; $j++) {
            if ($temp == 0)
                if ($j % 2 == 0)
                    $class = 'whiteCell';
                else
                    $class = 'blackCell';
            else
                if ($j % 2 == 0)
                    $class = 'blackCell';
                else
                    $class = 'whiteCell';

            $number = 0;
            $img = 0;
            if (isset($position[$i][$j])) {

                echo "<td class='$class'>";

                if ($position[$i][$j] == 3) {
                    $img = 'blackChecker';
                    $number = $blackCount++;
                } elseif ($position[$i][$j] == 1) {
                    $img = 'whiteChecker';
                    $number = $whiteCount++;
                } elseif ($position[$i][$j] == 4) {
                    $img = 'blackQueenChecker';
                    $number = $blackQueenCount++;
                } elseif ($position[$i][$j] == 2) {
                    $img = 'whiteQueenChecker';
                    $number = $whiteQueenCount++;
                }

                //Чтобы можно было выделять только твои шашки.
                if (array_search($img, $youClasses) === false)
                    echo "<img class = 'checker js$img' src='../img/$img.png'>"; //onclick='clickEnemy(\"js$img\",$number,$i,$j)'
                else
                    echo "<img class = 'checker js$img' src='../img/$img.png' onclick='clickAlly(\"js$img\",$number,$i,$j)'>";
            } else
                if ($class == 'blackCell')
                    echo "<td class='$class' onclick='clickEmpty($i,$j)'>";
                else
                    echo "<td class='$class'>";

            echo "</td>";
        }
        echo "</tr>";
    }
    echo "<input type='hidden' id='step' value='$turn'>";
}

/**
 * Обновление информации об игре.
 */
function getNewInfo()
{
    if (!isset($_POST['gameId'])) {
        echo 'ERROR';
        return;
    }

    global $dbHostname, $dbDatabase, $dbUsername, $dbPassword;
    $dbServer = mysqli_connect($dbHostname, $dbUsername, $dbPassword);
    if (!$dbServer) die(mysqli_error($dbServer));
    mysqli_select_db($dbServer, $dbDatabase);
    mysqli_set_charset($dbServer, 'utf8');

    $gameId = mysql_entities_fix_string($_POST['gameId']);

    $query = mysqli_query($dbServer, "SELECT * FROM `GAME` WHERE `GAMEID`=$gameId") or die(mysqli_error($dbServer));
    if (mysqli_num_rows($query) < 1) {
        echo "ERROR";
        mysqli_close($dbServer);
        return;
    }

    $game = mysqli_fetch_row($query);

    if (!isset($game[1]))
        $game[1] = "Ожидание противника";
    if (!isset($game[2]))
        $game[2] = "Ожидание противника";

    $step = "Ожидание входа противника";

    if ($game[4] == 1)
    {
        $step = "Ходит $game[1]";
    }elseif($game[4] == -1)
    {
        $step = "Ходит $game[2]";
    }

    echo "<div><div>$game[1]</div><div><img src='../img/whiteChecker.png'></div></div>
          <div><div>$game[2]</div><div><img src='../img/blackChecker.png'></div></div>
          <div>$step</div>";
}

function checkEndOfGame($state)
{
    $firstCount = 0;
    $secondCount = 0;
    foreach ($state as $row)
        foreach($row as $cell)
            if (isset($cell))
                if ($cell == 1 || $cell == 2)
                    $firstCount++;
                else
                    $secondCount++;

    if ($firstCount == 0 || $secondCount == 0)
        return true;
    else
        return false;
}