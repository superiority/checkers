<?php
/**
 * Created by PhpStorm.
 * User: Feodor
 * Date: 28.11.2015
 * Time: 17:28
 */

/**
 * @param $string
 * @return string
 */
function mysql_entities_fix_string($string)
{
    if (get_magic_quotes_gpc())
        $string = stripslashes($string);
    mysql_real_escape_string($string);

    return htmlentities($string);
}