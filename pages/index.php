<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <title>Checkers</title>
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../bootstrap/css/bootstrap-theme.min.css">
    <link rel="stylesheet" type="text/css" href="../css/styles.css">
    <!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>-->
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../scripts/scripts.js"></script>
    <script src="../scripts/jquery-1.11.3.min.js"></script>
</head>
<body id="table">
<?php
require_once 'login.php';
require_once 'functions.php';

global $dbHostname, $dbDatabase, $dbUsername, $dbPassword;

$dbServer = mysqli_connect($dbHostname, $dbUsername, $dbPassword);
if (!$dbServer) die(mysqli_error($dbServer));
mysqli_select_db($dbServer, $dbDatabase);
mysqli_set_charset($dbServer, 'utf8');

$newGame = true;
$youClasses = array('whiteChecker', 'whiteQueen');
$username = 'Guest';
$enemyName = 'Unknown';
$firstPlayer = 'Unknown';
$gameId = null;
$game = null;
$startGame = true;
/*
 * 1 - белая
 * 2 - белая коронка
 * 3 - черная
 * 4 - черная коронка
 */
$position = array();
$whiteCount = $blackCount = $whiteQueenCount = $blackQueenCount = 0;

if (isset($_COOKIE['username']))
    $username = $_COOKIE['username'];

if (isset($_COOKIE['gameId'])) {
    //Если уже играем загружаем инфу из бд.
    $gameId = $_COOKIE['gameId'];
    $newGame = false;
    loadGame($gameId);
}
else {
    //Поиск не завершенной игры.
    $sql = "SELECT
                `gameId`
            FROM
                `game`
            WHERE
                (
                    `Player1` = '$username'
                    OR `Player2` = '$username'
                )
                AND (
                    `turn` <> '0'
                    OR `turn` IS NULL
                )";
    $searchGame = mysqli_query($dbServer, $sql) or die(mysqli_error($dbServer) . 'Поиск не завершенных игр.');
    if (mysqli_num_rows($searchGame) > 0)
    {
        $newGame = false;
        $gameId = mysqli_fetch_row($searchGame)[0];
        setcookie('gameId',$gameId );
        loadGame($gameId);
    }
    else {
        $searchGame = mysqli_query($dbServer, "SELECT * FROM `GAME` WHERE `PLAYER2` IS NULL OR `PLAYER1` IS NULL") or die(mysqli_error($dbServer) . 'Поиск открытых игр.');
        //Если есть открытая новая игра.
        if (mysqli_num_rows($searchGame) > 0) {
            $game = mysqli_fetch_row($searchGame);
            while($game) {
                if ($game[1] != $username && $game[2] != $username)
                {
                    $gameId = $game[0];
                    if ($game[1] === null) {
                        $game[1] = $username;
                        //echo "first";
                        $firstPlayer = $username;
                        $youClasses = array('whiteChecker', 'whiteQueen');
                        mysqli_query($dbServer, "UPDATE `game` SET `Player1`='$username', `turn`=1 WHERE `gameId`=$gameId") or die(mysqli_error($dbServer) . "Вход первым игроком.");
                    } elseif (!isset($game[2])) {
                        //echo "second";
                        $game[2] = $username;
                        $firstPlayer = $game[1];
                        $youClasses = array('blackChecker', 'blackQueen');
                        mysqli_query($dbServer, "UPDATE `game` SET `Player2`='$username', `turn`=1 WHERE `gameId`=$gameId") or die(mysqli_error($dbServer) . "Вход вторым игроком.");
                    } else die("Произошла ошибка нахождение игры.");

                    setcookie('gameId', $gameId);
                    $startGame = false;
                    break;
                }
                else
                    $game = mysqli_fetch_row($searchGame);
            }

        } elseif ($startGame) {
            mysqli_query($dbServer, "INSERT INTO `game`(`Player1`) VALUES ('$username')") or die(mysqli_error($dbServer) . "Создание новой игры.");
            $game[1] = $username;
            $gameId = mysqli_insert_id($dbServer);
            setcookie('gameId', $gameId);
        }
    }
}
?>
<div class="container general">
    <span class="btn-group" role="group" aria-label="...">
            <a href="../">
                <button type="button" class="btn btn-default">На главную</button>
            </a>
    </span>
    <table id="pole">
        <tbody id="tableBody">
        <?php

        if (!isset($game[1]))
            $game[1] = "Ожидание противника";
        if (!isset($game[2]))
            $game[2] = "Ожидание противника";

        echo "<div class='info' id='info'><div><div>$game[1]</div><div><img src='../img/whiteChecker.png'></div></div>
                    <div><div>$game[2]</div><div><img src='../img/blackChecker.png'></div></div>
                    <div id='currentTurn'></div></div>";

        for ($i = 0; $i < 8; $i++) {
            $temp = $i % 2;
            for ($j = 0; $j < 8; $j++) {
                if ($temp == 0)
                    if ($j % 2 == 0)
                        $class = 'whiteCell';
                    else
                        $class = 'blackCell';
                else
                    if ($j % 2 == 0)
                        $class = 'blackCell';
                    else
                        $class = 'whiteCell';

                if ($newGame) {
                    if ($class == 'blackCell' && $i < 3)
                        $position[$i][$j] = 3;
                    if ($class == 'blackCell' && $i > 4)
                        $position[$i][$j] = 1;
                }

                $number = 0;
                if (isset($position[$i][$j])) {
                    echo "<td class='$class'>";

                    if ($position[$i][$j] == 3) {
                        $img = 'blackChecker';
                        $number = $blackCount++;
                    }
                    elseif ($position[$i][$j] == 1) {
                        $img = 'whiteChecker';
                        $number = $whiteCount++;
                    }
                    elseif ($position[$i][$j] == 4) {
                        $img = 'blackQueenChecker';
                        $number = $blackQueenCount++;
                    }
                    elseif ($position[$i][$j] == 2) {
                        $img = 'whiteQueenChecker';
                        $number = $whiteQueenCount++;
                    }

                    //Чтобы можно было выделять только твои шашки.
                    if (array_search($img, $youClasses) === false)
                        echo "<img class = 'checker js$img jsEnemy' src='../img/$img.png' onclick='clickEnemy(\"js$img\",$number,$i,$j)'>";
                    else
                        echo "<img class = 'checker js$img jsAlly' src='../img/$img.png' onclick='clickAlly(\"js$img\",$number,$i,$j)'>";
                } else
                    if ($class == 'blackCell')
                        echo "<td class='$class' onclick='clickEmpty($i,$j)'>";
                    else
                        echo "<td class='$class'>";

                echo "</td>";
            }
            echo "</tr>";
        }
        $json = json_encode($position);
        mysqli_query($dbServer,"UPDATE `game` SET `state`='$json' WHERE `gameId`=$gameId") or die(mysqli_error($dbServer));
        mysqli_close($dbServer);
        ?>
        </tbody>
    </table>
</div>
</body>
<script>update();</script>
</html>
<?php
function loadGame($gameId)
{
    global  $dbServer, $username, $game, $position, $firstPlayer, $youClasses, $enemyName;

    $query = mysqli_query($dbServer, "SELECT * FROM `GAME` WHERE `gameId`=$gameId") or die(mysqli_error($dbServer) . 'Попытка загрузки игры.');
    if (mysqli_num_rows($query) < 1)
    {
        setcookie ("gameId", "", time() - 3600);
        header("Location: index.php");
    }

    $game = mysqli_fetch_row($query);
    if ($game[4] === 0 || $game[5] !== null)
    {
        setcookie ("gameId", "", time() - 3600);
        header("Location: ../index.php");
    }

    $position = json_decode($game[3],true);
    $firstPlayer = $game[1];

    if ($game[1] == $username) {
        $youClasses = array('whiteChecker', 'whiteQueen');
        $enemyName = $game[2];
    }
    elseif ($game[2] == $username) {
        $youClasses = array('blackChecker', 'blackQueen');
        $enemyName = $game[1];
    }
    elseif($game[2] == null)
    {
        //Чтобы можно было играть с одного компа.
        mysqli_query($dbServer, "UPDATE `game` SET `Player2`='$username', `turn`=1 WHERE `gameId`=$gameId") or die(mysqli_error($dbServer) . "Вход вторым игроком.");
        $youClasses = array('blackChecker', 'blackQueen');
        $enemyName = $game[1];
    }
    else
    {
        setcookie ("gameId", "", time() - 3600);
        header("Location: index.php");
    }
}

function checkEndOfGame($gameId)
{
    global  $dbServer;
    $query = mysqli_query($dbServer, "SELECT `WINNER` FROM `GAME` WHERE `gameId`=$gameId") or die(mysqli_error($dbServer) . 'Попытка загрузки игры.');
    if (mysqli_fetch_row($query)[0] !== null)
    {
        setcookie ("gameId", "", time() - 3600);
        mysqli_close($dbServer);
        header("Location: ../index.php");
    }
}
?>